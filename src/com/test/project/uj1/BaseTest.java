package com.test.project.uj1;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public abstract class BaseTest {

    protected static WebDriver driver;
    //selenum lubi jak driver jest statyczny

    //ponizsze 2 klasy dziedziczymy zgodnie z metodyką DRY nie powtarzamy


    @AfterMethod
    public void tearDownMethod(ITestResult result) throws IOException {
        //jesli failt rob zrzut
        if (!result.isSuccess()) {
            File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenshot, new File(
                    BaseProperties.screenshotsPath + "screenshot_"
                            + result.getMethod().getMethodName()
                            + "_"
                            + new Date().getTime()
                            + ".png"));
        }
    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }

    @BeforeTest
    @Parameters("browser")

    public void setUp(String browser) {

        BaseWebDriver baseWebDriver = new BaseWebDriver(); // odwolujemy sie do obiektu
        driver = baseWebDriver.getBaseWebDriver(browser);
        driver.get(BaseProperties.applicationUrl);

    }


}
