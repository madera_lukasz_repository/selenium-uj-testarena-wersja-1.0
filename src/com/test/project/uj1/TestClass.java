package com.test.project.uj1;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;


public class TestClass extends BaseTest {

    @Test
    public void loginTest() {
        driver.findElement(By.id("email")).sendKeys(BaseProperties.administratorLogin); //odwolujemy sie do klasy BaseProperties oraz obiektu administratorLogin
        driver.findElement(By.id("password")).sendKeys(BaseProperties.getAdministratorPassword);
        driver.findElement(By.id("login")).click();

        Assert.assertTrue(driver.findElement(By.xpath("//html/body/header/div[2]/div[2]/a/span")).isDisplayed());


    }

}
