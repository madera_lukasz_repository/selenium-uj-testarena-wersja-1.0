package com.test.project.uj1;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Date;


public class TestClass3 extends BaseTest {

    @Test
    public void addVersionsTest() {
        //wejscie na srodowisko
        driver.findElement(By.xpath("//*[@id=\"wrapper\"]/ul/li[5]")).click();
        //driver.findElement(By.className("item5")).click();
        //klikniecie d odaj srodowisko
        driver.findElement(By.className("button_link")).click();

        //wypelnienie pola nazwa
        driver.findElement(By.id("name")).sendKeys("Nazwa LM1" + new Date().getTime());

        //wypelnienie pola opis
        driver.findElement(By.id("description")).sendKeys("Opis wersji LM1" + new Date().getTime());

        //klikniecie zapisz
        driver.findElement(By.id("save")).click();

        //oczekiwanie na wyswietlenie

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("#j_info_box p"), "Version successfully added.")); // ewentualnie elementToBeClickable
        //sprawdzenie czy sie udalo dodac
        //sprawdzneie EN, w przypadku wybranego jezyka PL nie zadziala
//        try {
//            Thread.sleep(5000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        //ten element się nie załadował dlatego daliśmy wyzej opoznienie
        Assert.assertEquals(driver.findElement(By.cssSelector("#j_info_box p")).getText(), "Version successfully added.");
    }
}
