package com.test.project.uj1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class BaseWebDriver {

    private WebDriver driver;

    public WebDriver getBaseWebDriver(String browser) {
        System.out.println(browser);
        if (browser.equals("firefox")) {
            System.setProperty("webdriver.gecko.driver", BaseProperties.firefoxDriverPath);
            driver = new FirefoxDriver();
        } else if (browser.equals("chrome")) {
            System.setProperty("webdriver.chrome.driver", BaseProperties.chromeDriverPath);
            driver = new ChromeDriver();
        } else {
            //(browser.equals("firefox"))
            System.setProperty("webdriver.gecko.driver", BaseProperties.firefoxDriverPath);
            driver = new FirefoxDriver();
        }

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        return driver;
    }

    public WebDriver getCustomBaseWebDriver() {
//Metoda zwracajaca customowego drivera w dowolnej konfiguracji
/*      System.setProperty("webdriver.gecko.driver","C:\\selendriver\\geckodriver.exe");
        //System.setProperty("webdriver.chrome.driver","C:\\selendriver\\chromedriver.exe");
        driver = new FirefoxDriver();
        //driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();*/

        return driver;
    }
}
